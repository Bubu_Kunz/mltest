package com.yanzubritskiy.mltest

import android.content.Intent

class EntryChoiceActivity : BaseEntryChoiceActivity() {
    override val choices: List<Choice>
        get() = listOf(
            Choice(
                "Java",
                "Run the Firebase ML Kit quickstart written in Java.",
                Intent(this,
                    com.yanzubritskiy.mltest.java.ChooserActivity::class.java)),
            Choice(
                "Kotlin",
                "Run the Firebase ML Kit quickstart written in Kotlin.",
                Intent(
                    this,
                    com.yanzubritskiy.mltest.kotlin.ChooserActivity::class.java))
        ) //To change initializer of created properties use File | Settings | File Templates.

}
