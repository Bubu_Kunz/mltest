package com.yanzubritskiy.mltest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

abstract class BaseEntryChoiceActivity : AppCompatActivity() {

    private var mRecycler: RecyclerView? = null

    protected abstract val choices: List<Choice>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entry_choice)

        mRecycler = findViewById(R.id.choices_recycler)
        mRecycler!!.setLayoutManager(LinearLayoutManager(this))
        mRecycler!!.setAdapter(ChoiceAdapter(this, choices))
    }
}
