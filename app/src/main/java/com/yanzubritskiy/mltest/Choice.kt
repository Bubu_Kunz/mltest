package com.yanzubritskiy.mltest

import android.content.Intent

class Choice(val title: String, val description: String, val launchIntent: Intent)